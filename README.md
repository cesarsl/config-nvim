# .config/nvim

An attenpt in building a small but functional neovim init files (mostly) in Lua.

![GitLab](https://img.shields.io/gitlab/license/cesarsl/config-nvim?style=flat-square)

## Requirements

  - packer.nvim
  - nodejs (for LSP)
  - neovim >= 0.7

## Organizing folders and files

```bash
.config/nvim
├── init.lua
├── lua
│   ├── core
│   │   ├── autocmds.lua
│   │   ├── globals.lua
│   │   ├── keybinds.lua
│   │   └── options.lua
│   ├── lsp
│   │   ├── init.lua
│   │   ├── servers
│   │   │   └── <lang-server>.lua
│   │   └── util.lua
│   ├── plugins
│   │   ├── init.lua
│   │   ├── setup
│   │   │   └── <plugin_name>.lua
│   │   └── startup
│   │       └── <plugin_name>.lua
│   └── util
│       └── fs.lua
└── plugin # packer.nvim creates this
    └── packer_compiled.lua
```
