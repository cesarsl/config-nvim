local map = vim.api.nvim_set_keymap

local opts = { noremap = true, silent = true }

map('n', '<C-h>', '<C-w>h', opts)
map('n', '<C-j>', '<C-w>j', opts)
map('n', '<C-k>', '<C-w>k', opts)
map('n', '<C-l>', '<C-w>l', opts)

map('n', '<S-l>', '<Cmd>bnext<CR>', opts)
map('n', '<S-h>', '<Cmd>bprevious<CR>', opts)
map('n', '<C-w>', '<Cmd>bdelete<CR>', opts)
map('n', '<C-s>', '<Cmd>write<CR>', opts)

map('i', 'jk', '<Esc>', opts)

map('v', '<', '<gv', opts)
map('v', '>', '>gv', opts)

map('n', '<A-j>', '<Cmd>m .+1<CR>==', opts)
map('n', '<A-k>', '<Cmd>m .-2<CR>==', opts)

map('x', 'ga', '<Plug>(EasyAlign)<CR>', opts)
map('n', 'ga', '<Plug>(EasyAlign)<CR>', opts)
