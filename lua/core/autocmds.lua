local api = vim.api
local opt = vim.opt

api.nvim_create_augroup('Misc', { clear = true })

api.nvim_create_autocmd('TermOpen', {
  group = 'Misc',
  pattern = '*',
  callback = function()
    opt.number         = false
    opt.relativenumber = false
    opt.signcolumn     = 'no'
  end
})

api.nvim_create_autocmd('ColorScheme', {
  group = 'Misc',
  pattern = '*',
  callback = function()
    api.nvim_command('hi Normal ctermbg=NONE guibg=NONE')
    api.nvim_command('hi SignColumn ctermbg=NONE guibg=NONE')
  end
})
