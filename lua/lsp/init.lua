local api = vim.api
local dgn = vim.diagnostic
local lsp = vim.lsp
local map = vim.keymap.set

local fs = require('util.fs')
local lspconfig = require('lspconfig')

local servers_list = fs.list_filenames_in_dir('lua/lsp/servers/*.lua')

local group = api.nvim_create_augroup('LSP', { clear = true })

for _, server in pairs(servers_list) do
  local module = require('lsp.servers.' .. server)
  lspconfig[server].setup(module)
end

local opts = { noremap = true, silent = true }

map('n', '<Leader>e', dgn.open_float, opts)
map('n', '<Leader>q', dgn.setloclist, opts)
map('n', '[d', dgn.goto_prev, opts)
map('n', ']d', dgn.goto_next, opts)

api.nvim_create_autocmd('LspAttach', {
  group = group,
  callback = function(args)
    api.nvim_buf_set_option(args.buf, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

    local buf_opts = { noremap = true, silent = true, buffer = args.buf }

    map('n', '<Leader>D', lsp.buf.type_definition, buf_opts)
    map('n', '<Leader>ca', lsp.buf.code_action, buf_opts)
    map('n', '<Leader>f', function() lsp.buf.format({ async = true }) end, buf_opts)
    map('n', '<Leader>rn', lsp.buf.rename, buf_opts)
    map('n', '<Leader>wa', lsp.buf.add_workspace_folder, buf_opts)
    map('n', '<Leader>wl', function() vim.pretty_print(lsp.buf.list_workspace_folders()) end, buf_opts)
    map('n', '<Leader>wr', lsp.buf.remove_workspace_folder, buf_opts)
    map('n', 'gD', lsp.buf.declaration, buf_opts)
    map('n', 'gd', lsp.buf.definition, buf_opts)
    map('n', 'gh', lsp.buf.hover, buf_opts)
    map('n', 'gi', lsp.buf.implementation, buf_opts)
    map('n', 'gr', lsp.buf.references, buf_opts)
    map('n', 'gs', lsp.buf.signature_help, buf_opts)
  end,
})
