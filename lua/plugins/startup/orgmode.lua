local M = {
  'nvim-orgmode/orgmode',
  config = function()
    require('orgmode').setup()
    require('orgmode').setup_ts_grammar()
  end
}

return M
