local M = {
  'w0ng/vim-hybrid',
  config = function() vim.cmd('colorscheme hybrid') end
}

return M
