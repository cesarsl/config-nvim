local ok, M = pcall(require, 'nvim-treesitter.configs')
if not ok then
  return
end

M.setup({
  ensure_installed = {
    'bash', 'css', 'dockerfile', 'go', 'gomod', 'html', 'http', 'javascript', 'json',
    'latex', 'lua', 'make', 'markdown', 'org', 'python', 'regex', 'scss', 'sql',
    'svelte', 'toml', 'tsx', 'typescript', 'vim', 'yaml'
  },
  highlight = {
    enable = true,
  },
  additional_vim_regex_highlighting = { 'org' },
})

return M
