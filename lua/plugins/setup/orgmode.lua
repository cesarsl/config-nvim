local ok, M = pcall(require, 'orgmode')
if not ok then
  return
end

M.setup({
  org_agenda_files = { '~/OneDrive/org/**/*' },
  org_default_notes_file = '~/OneDrive/org/notes.org',
})

return M
