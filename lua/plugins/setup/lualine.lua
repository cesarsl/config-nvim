local ok, M = pcall(require, 'lualine')
if not ok then
  return
end

M.setup({
  options = {
    theme = 'codedark'
  }
})

return M
