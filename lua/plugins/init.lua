local ok, M = pcall(require, 'packer')
if not ok then
  return
end

local fs = require('util.fs')

local startup_list = fs.list_filenames_in_dir('lua/plugins/startup/*.lua')
local setup_list = fs.list_filenames_in_dir('lua/plugins/setup/*.lua')

M.startup(function(use)
  for _, name in pairs(startup_list) do
    local config = require('plugins.startup.' .. name)
    use(config)
  end
end)

for _, name in pairs(setup_list) do
  require('plugins.setup.' .. name)
end

return M
