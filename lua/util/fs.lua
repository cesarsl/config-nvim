local M = {}

local api = vim.api

function M.extract_file_basename(file_path)
  local basename = vim.split(file_path, '/', true)
  basename = basename[#basename]
  return vim.split(basename, '.', true)[1]
end

function M.list_filenames_in_dir(path)
  local names = {}
  local files = api.nvim_get_runtime_file(path, true)
  for _, file_path in pairs(files) do
    table.insert(names, M.extract_file_basename(file_path))
  end
  return names
end

return M
