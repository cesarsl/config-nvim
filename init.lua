require('core.options')
require('core.globals')
require('core.autocmds')
require('core.keybinds')

require('plugins')
require('lsp')
